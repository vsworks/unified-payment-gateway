package com.app.upg.rest;

import com.app.upg.services.Gateway;
import com.app.upg.services.GatewayServices;
import com.app.upg.services.impl.payu.PayUServiceImpl;
import com.app.upg.services.impl.payuMoney.PayUMoneyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Hi on 09-08-2018.
 */
@Service
public class GatewayManager {

    @Autowired
    PayUMoneyServiceImpl payUMoneyService;

    @Autowired
    PayUServiceImpl payUService;

    public GatewayServices getGateway(Gateway gateway)
    {
       // Gateway gatewayRequested=Gateway.valueOf(gateway);
        if(gateway==Gateway.PAYU)
        {
            return payUService;
        }
        else if(gateway==Gateway.PAYU_MONEY)
        {
            return payUMoneyService;
        }
        else
        {
            return  payUService;
        }

    }

    public GatewayServices getGateway()
    {
        return getGateway(null);
    }
}
