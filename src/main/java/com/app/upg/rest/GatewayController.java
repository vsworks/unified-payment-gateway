/**
 * Class will work as rest controller for user manager.
 */
package com.app.upg.rest;

import com.app.upg.model.Order;
import com.app.upg.model.Payment;
import com.app.upg.services.GatewayServices;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Vaibhav Jatar
 *
 */
@RestController
@RequestMapping("/payment")
public class GatewayController {
	static Logger LOGGER = Logger.getLogger(GatewayController.class);

	@Autowired
	GatewayManager gatewayManager;



	/**
	 * Ping service to identify if Application is up and running
	 * @return
	 */
	@RequestMapping(value = "/ping", method = RequestMethod.GET)
	public ResponseEntity<String> ping() {
		LOGGER.info("Ping request recieved");
		return new ResponseEntity<String>("Backend server is up and running", HttpStatus.OK);

	}

	@RequestMapping(value = "/order", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<String> Order(@RequestBody Order order)  {
		String response= "Failed";
		try {
			LOGGER.info("Order request recieved");
			GatewayServices gatewayServices=gatewayManager.getGateway(order.getGateway());
			response = gatewayServices.setOrder(order);
		} catch (Exception e) {
			LOGGER.error(e);
			e.printStackTrace();
			return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(response, HttpStatus.OK);

	}

	@RequestMapping(value = "/status/transactionId/{transactionId}", method = RequestMethod.GET)
	public ResponseEntity<Payment> status(@PathVariable String transactionId) {
		LOGGER.info("Reuest recieved to check status TranscationId :: "+transactionId);
		Payment payment=null;
		try {
			GatewayServices gatewayServices=gatewayManager.getGateway();
		 payment=gatewayServices.getPaymentStatus(transactionId);
		} catch (Exception e) {
		LOGGER.error(e);
		e.printStackTrace();
		return new ResponseEntity<Payment>(payment, HttpStatus.INTERNAL_SERVER_ERROR);
	}
		return new ResponseEntity<Payment>(payment, HttpStatus.OK);

	}

	@RequestMapping(value = "/callback", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<String> callback(@RequestParam String transcationId) {
		try {
			LOGGER.info("Callback received");
			LOGGER.debug("Order Id Completed for Consumer Key "+transcationId);
			GatewayServices gatewayServices=gatewayManager.getGateway();
			gatewayServices.paymentCallBack(transcationId);
		} catch (Exception e) {
			LOGGER.error(e);
			e.printStackTrace();
			return new ResponseEntity<String>("Callback", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>("Callback", HttpStatus.OK);

	}
}
