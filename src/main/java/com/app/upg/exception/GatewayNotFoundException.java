package com.app.upg.exception;

@SuppressWarnings("serial")
public class GatewayNotFoundException extends RuntimeException {

	public GatewayNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}


	public GatewayNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GatewayNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GatewayNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
