package com.app.upg.dao;

import com.app.upg.dao.bean.Transcation;

public interface TranscationDao {


    void save(Transcation transcation);

    Transcation getTranscation(String consumerUniqueKey);

    void update(Transcation transcation);
}
