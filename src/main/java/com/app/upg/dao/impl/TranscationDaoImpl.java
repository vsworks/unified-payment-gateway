package com.app.upg.dao.impl;

import com.app.upg.dao.TranscationDao;
import com.app.upg.dao.bean.Transcation;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TranscationDaoImpl implements TranscationDao {
	static Logger LOGGER = Logger.getLogger(TranscationDaoImpl.class);

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	SessionFactory sessionFactory;

	public void save(Transcation transcation) {
		hibernateTemplate.save(transcation);
	}

	@Transactional
	public Transcation getTranscation(String consumerUniqueKey) {
		List<Transcation> transcation = (List<Transcation>) hibernateTemplate.find("select t from Transcation t where customId=?",consumerUniqueKey);
		if (transcation.size()<1) {
			LOGGER.error("Trancation not found");
			throw new RuntimeException();
		}
		return  transcation.get(0);
	}

	@Transactional
	public void update(Transcation transcation) {
		try {
			hibernateTemplate.saveOrUpdate(transcation);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
