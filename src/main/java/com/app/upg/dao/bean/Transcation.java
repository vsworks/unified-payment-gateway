/**/
package com.app.upg.dao.bean;

import javax.persistence.*;
import java.sql.Date;

/**
 * @author Vaibhav Jatar
 *
 */
@Entity
@Table(name = "transcation", uniqueConstraints = {
		@UniqueConstraint(columnNames = "transactionId") })
public class Transcation {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String transactionId;
	private double amount;
	private String currency;
	private String gateway;
	private String email;
	private String bankDetails;
	private String userName;
	private String customId;
	private Date createdDate;
	private Date modifiedDate;
	private String status;
	public Transcation(){}
	public Transcation(String transactionId, double amount) {
		this.transactionId = transactionId;
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(String bankDetails) {
		this.bankDetails = bankDetails;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCustomId() {
		return customId;
	}

	public void setCustomId(String customId) {
		this.customId = customId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public static final class TranscationBuilder {
		private int id;
		private String transactionId;
		private double amount;
		private String currency;
		private String gateway;
		private String email;
		private String bankDetails;
		private String userName;
		private String customId;
		private Date createdDate;
		private Date modifiedDate;
		private String status;

		private TranscationBuilder() {
		}

		public static TranscationBuilder getTranscation() {
			return new TranscationBuilder();
		}

		public TranscationBuilder withId(int id) {
			this.id = id;
			return this;
		}

		public TranscationBuilder withTransactionId(String transactionId) {
			this.transactionId = transactionId;
			return this;
		}

		public TranscationBuilder withAmount(double amount) {
			this.amount = amount;
			return this;
		}

		public TranscationBuilder withCurrency(String currency) {
			this.currency = currency;
			return this;
		}

		public TranscationBuilder withGateway(String gateway) {
			this.gateway = gateway;
			return this;
		}

		public TranscationBuilder withEmail(String email) {
			this.email = email;
			return this;
		}

		public TranscationBuilder withBankDetails(String bankDetails) {
			this.bankDetails = bankDetails;
			return this;
		}

		public TranscationBuilder withUserName(String userName) {
			this.userName = userName;
			return this;
		}

		public TranscationBuilder withCustomId(String customId) {
			this.customId = customId;
			return this;
		}

		public TranscationBuilder withStatus(String status) {
			this.status = status;
			return this;
		}

		public TranscationBuilder withCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
			return this;
		}

		public TranscationBuilder withModifiedDate(Date modifiedDate) {
			this.modifiedDate = modifiedDate;
			return this;
		}

		public Transcation build() {
			Transcation transcation = new Transcation();
			transcation.setId(id);
			transcation.setTransactionId(transactionId);
			transcation.setAmount(amount);
			transcation.setCurrency(currency);
			transcation.setGateway(gateway);
			transcation.setEmail(email);
			transcation.setBankDetails(bankDetails);
			transcation.setUserName(userName);
			transcation.setCustomId(customId);
			transcation.setCreatedDate(createdDate);
			transcation.setModifiedDate(modifiedDate);
			transcation.setStatus(status);
			return transcation;
		}
	}
}