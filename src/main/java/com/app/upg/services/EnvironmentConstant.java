package com.app.upg.services;

/**
 * Created by Hi on 15-08-2018.
 */
public class EnvironmentConstant {

    public static final String PAYMENT_INITIATED = "PAYMENT_INITIATED";
    public static final String PAYMENT_ORDER_SET = "PAYMENT_ORDER_SET";
    public static final String PAYMENT_SUCCESSFUL = "PAYMENT_SUCCESSFUL";
}
