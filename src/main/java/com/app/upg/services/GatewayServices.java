package com.app.upg.services;

import com.app.upg.model.Order;
import com.app.upg.model.Payment;

import java.io.IOException;

public interface GatewayServices {

    String setOrder(Order order) throws IOException;

    Payment getPaymentStatus(String transcationId);

    void paymentCallBack(String transcationId);

}
