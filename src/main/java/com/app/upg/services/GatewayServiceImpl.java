package com.app.upg.services;

import com.app.upg.dao.TranscationDao;
import com.app.upg.dao.bean.Transcation;
import com.app.upg.model.Payment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public abstract  class GatewayServiceImpl implements GatewayServices {
	static Logger LOGGER = Logger.getLogger(GatewayServiceImpl.class);
	@Autowired
	TranscationDao transcationDao;

	public Payment getPaymentStatus(String transcationId) {
		Transcation transcation=transcationDao.getTranscation(transcationId);
		Payment payment=new Payment(transcation.getStatus(),transcation.getTransactionId(),transcation.getAmount(),transcation.getGateway());
		return payment;
	}

	public void paymentCallBack(String transcationId) {
		Transcation transcation=transcationDao.getTranscation(transcationId);
		transcation.setStatus(EnvironmentConstant.PAYMENT_SUCCESSFUL);
		transcationDao.update(transcation);
	}
}
