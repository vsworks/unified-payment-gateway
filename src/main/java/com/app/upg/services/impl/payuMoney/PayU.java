package com.app.upg.services.impl.payuMoney;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

//import sun.net.www.http.HttpClient;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author root
 */
@Component
@PropertySource(value = { "classpath:payu.properties" })
public class PayU {
    static Logger LOGGER = Logger.getLogger(PayU.class);
    private Integer error;
    @Autowired
    private Environment environment;

    public boolean empty(String s) {
        if (s == null || s.trim().equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }

        } catch (NoSuchAlgorithmException nsae) {
        }
        return hexString.toString();
    }

    public Map<String, String> hashCalMethod(int applicationFee)
            throws ServletException, IOException {
        String salt = environment.getRequiredProperty("salt");//"eCwWELxi";
        String action1 = "";
        String base_url = environment.getRequiredProperty("payuUrl");// "https://test.payu.in";
        error = 0;
        String hashString = "";
       // Enumeration paramNames = request.getParameterNames();
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String> urlParams = new HashMap<String, String>();
       /* while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();
            String paramValue = request.getParameter(paramName);
            params.put(paramName, paramValue);
        }*/
      //  Guardian guardian=application.getApplicant().getGuardian().get(0);
      //  params.put("key","gtKFFx");
        params.put("key", environment.getRequiredProperty("key"));
        params.put("amount",""+applicationFee);
        params.put("productinfo",environment.getRequiredProperty("productinfo"));
        params.put("firstname","");
        params.put("email","");
        params.put("phone","");
        params.put("surl",environment.getRequiredProperty("surl"));//"http://localhost:9966/onlineportal/redirect/success");
        params.put("furl",environment.getRequiredProperty("furl"));//"http://localhost:9966/onlineportal/redirect/failure");
        params.put("service_provider","payu_paisa");
        String txnid = "";
        if (empty(params.get("txnid"))) {
            Random rand = new Random();
            String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            txnid = rndm;
            params.remove("txnid");
            params.put("txnid", txnid);
            txnid = hashCal("SHA-256", rndm).substring(0, 20);
        } else {
            txnid = params.get("txnid");
        }
        
        
        String txn = "abcd";
        String hash = "";
        String otherPostParamSeq = "phone|surl|furl|lastname|curl|address1|address2|city|state|country|zipcode|pg";
        String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if (empty(params.get("hash")) && params.size() > 0) {
            if (empty(params.get("key")) || empty(txnid) || empty(params.get("amount")) || empty(params.get("firstname")) || empty(params.get("email")) || empty(params.get("phone")) || empty(params.get("productinfo")) || empty(params.get("surl")) || empty(params.get("furl")) || empty(params.get("service_provider"))) {
                error = 1;
            } else {
                
                String[] hashVarSeq = hashSequence.split("\\|");
                for (String part : hashVarSeq) {
                    if (part.equals("txnid")) {
                        hashString = hashString + txnid;
                        urlParams.put("txnid", txnid);
                    } else {
                        hashString = (empty(params.get(part))) ? hashString.concat("") : hashString.concat(params.get(part).trim());
                        urlParams.put(part, empty(params.get(part)) ? "" : params.get(part).trim());
                    }
                    hashString = hashString.concat("|");
                }
                hashString = hashString.concat(salt);
                hash = hashCal("SHA-512", hashString);
                action1 = base_url.concat("/_payment");
                String[] otherPostParamVarSeq = otherPostParamSeq.split("\\|");
                for (String part : otherPostParamVarSeq) {
                    urlParams.put(part, empty(params.get(part)) ? "" : params.get(part).trim());
                }

            }
        } else if (!empty(params.get("hash"))) {
            hash = params.get("hash");
            action1 = base_url.concat("/_payment");
        }

        urlParams.put("hash", hash);
        urlParams.put("action", action1);
        urlParams.put("hashString", hashString);
        return urlParams;
    }

    static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
    static String urlEncodeUTF8(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
           /* sb.append(String.format("%s=%s",
                    urlEncodeUTF8(entry.getKey().toString()),
                    urlEncodeUTF8(entry.getValue().toString())
            ));*/
            sb.append(String.format("%s=%s",
                   entry.getKey().toString(),
                  entry.getValue().toString()
            ));
        }
        return sb.toString();
    }

   /* public PayUResponse getPayUrl(Application application, int applicationFee) throws ServletException, IOException {
        // HTTP Post request
        String url = "https://test.payu.in/_payment";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // Setting basic post request
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        Map<String, String> urlParams =hashCalMethod(application,applicationFee);
        String postJsonData = urlEncodeUTF8(urlParams);

        // Send post request
        con.setInstanceFollowRedirects(false);
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(postJsonData);
        wr.flush();
        wr.close();

        LOGGER.debug("nSending 'POST' request to URL : " + url);
        LOGGER.debug("Post Data : " + postJsonData);
        LOGGER.debug("Response Data : " + con.getResponseMessage());
        LOGGER.debug("Response Code : " + con.getResponseCode());
        LOGGER.debug("Response Url : " + con.getHeaderField("Location"));
        PayUResponse payUResponse=new PayUResponse();
        payUResponse.setUrl(con.getHeaderField("Location"));
        payUResponse.setTxnId(urlParams.get("txnid"));
        return payUResponse;
    }*/
}
