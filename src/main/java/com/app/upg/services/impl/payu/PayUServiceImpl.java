package com.app.upg.services.impl.payu;

import com.app.upg.dao.TranscationDao;
import com.app.upg.dao.bean.Transcation;
import com.app.upg.http.HttpController;
import com.app.upg.http.Request;
import com.app.upg.model.Order;
import com.app.upg.services.EnvironmentConstant;
import com.app.upg.services.GatewayServiceImpl;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

@Service
public class PayUServiceImpl extends GatewayServiceImpl{
	static Logger LOGGER = Logger.getLogger(PayUServiceImpl.class);

	@Autowired
	HttpController httpController;

	@Autowired
	TranscationDao transcationDao;

	public String setOrder(Order order) throws IOException {

		System.out.println(order);
		System.out.println("Order recieved for "+order.getConsumerUniqueKey());
		String uniqueKey = UUID.randomUUID().toString();
		order.setConsumerUniqueKey(uniqueKey);
		Transcation transcation=setTranscation(order);
		saveOrder(transcation);
		PayUOrder payUOrder=configurePayuOrder(order);
		Request request=Request.BuildRequest
				.getRequest()
				.withUrl("https://secure.snd.payu.com/api/v2_1/orders")
				.withRequestType("POST")
				.withBasicHeaderWithToken(httpController.getAuthToken())
				.withPostData(payUOrder)
				.build();
		String response=httpController.executePostRequest(request,true);
		PayUOrderResponse payUOrderResponse=new Gson().fromJson(response,PayUOrderResponse.class);
		updateOrderId(payUOrderResponse.getOrderId(),order.getConsumerUniqueKey(),transcation);
		return payUOrderResponse.getRedirectUri();
	}

	private void saveOrder(Transcation transcation) {
	try{
		transcationDao.save(transcation);
	}
	catch(Exception e) {
			LOGGER.error("Database Object Not saved");
			throw new RuntimeException();
	}
	}

	private Transcation setTranscation(Order order) {

	Transcation transcation= Transcation.TranscationBuilder.getTranscation()
				.withAmount(order.getAmount())
				.withCreatedDate(new Date(Calendar.getInstance().getTimeInMillis()))
				.withCurrency("PLN")
				.withEmail(order.getEmailId())
				.withModifiedDate(new Date(Calendar.getInstance().getTimeInMillis()))
				.withUserName(order.getFirstName())
				.withCustomId(order.getConsumerUniqueKey())
				.withGateway(order.getGateway().toString())
				.withStatus(EnvironmentConstant.PAYMENT_INITIATED)
				.build();
		return transcation;

	}

	private void updateOrderId(String orderId,String consumerUniqueKey,Transcation transcation) {
		System.out.println(orderId);
		System.out.println("Oder Id for Consumer Key "+consumerUniqueKey +"is "+orderId);
		//Transcation transcation=transcationDao.getTranscation(consumerUniqueKey);
		transcation.setStatus(EnvironmentConstant.PAYMENT_ORDER_SET);
		transcation.setTransactionId(orderId);
		transcationDao.update(transcation);

	}

	private PayUOrder configurePayuOrder(Order order) {
		PayUOrder payUOrder=new PayUOrder();
		payUOrder.setCurrencyCode("PLN");
		payUOrder.setCustomerIp("127.0.0.1");
		payUOrder.setDescription(order.getDescription());
		payUOrder.setMerchantPosId("337138");
		payUOrder.setNotifyUrl(order.getNotifyUrl());
		payUOrder.setTotalAmount(""+order.getAmount());
		Buyer buyer=new Buyer();
		buyer.setEmail(order.getEmailId());
		buyer.setFirstName(order.getFirstName());
		buyer.setLastName(order.getLastName());
		buyer.setLanguage("en");
		ArrayList< Products > products = new ArrayList < Products > ();
		Products product=new Products();
		product.setName("Fees");
		product.setQuantity(""+1);
		product.setUnitPrice(""+order.getAmount());
		products.add(product);
		payUOrder.setProducts(products);
		payUOrder.setBuyer(buyer);
		return payUOrder;

	}


}
