package com.app.upg.services;

/**
 * Created by Hi on 09-08-2018.
 */
public enum Gateway {

    PAYU("PAYU"),PAYU_MONEY("PAYU_MONEY");

    private final String name;

    Gateway(String name) {
        this.name=name;
    }

    Gateway getGateway(String gatewayName)
    {
        return this.valueOf(gatewayName);
    }
}
