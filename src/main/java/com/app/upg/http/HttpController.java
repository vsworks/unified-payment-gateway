package com.app.upg.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by Hi on 14-08-2018.
 */
@Service
public class HttpController {

    static Logger LOGGER = Logger.getLogger(HttpController.class);


    public String executePostRequest(Request request,boolean isConvertRequired)  {
        try {
        URL obj = new URL(request.getUrl());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // Setting basic post request
        con.setRequestMethod(request.getRequestType());
        setHeaders(con,request.getHeader());
        GsonBuilder gsonBuilder = new GsonBuilder();
        //gsonBuilder.registerTypeAdapter(Date.class, new Dateserializer());
            String postJsonData =null;
            if (isConvertRequired) {
                postJsonData = gsonBuilder.create().toJson(request.getPostData());
            }
            else {
                 postJsonData = (String)request.getPostData();
            }
            con.setInstanceFollowRedirects(false);
        con.setDoOutput(true);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(), "UTF-8"));
        bw.write(postJsonData);
        bw.flush();
        bw.close();
        LOGGER.debug("nSending 'POST' request to URL : " + request.getUrl());
        LOGGER.debug("Post Data : " + postJsonData);
        LOGGER.debug("Response Data : " + con.getResponseMessage());
        LOGGER.debug("Response Code : " + con.getResponseCode());
        StringBuilder sb = new StringBuilder();
        switch (con.getResponseCode()) {
            case 200:
            case 302:
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                LOGGER.info("Response:"+sb.toString());
                System.out.println(sb.toString());
                return sb.toString();
            default: LOGGER.error("Request Failed with status code " + con.getResponseCode());
        }

    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
}



    public String getAuthToken() throws IOException {

        Request request=Request.BuildRequest
                .getRequest()
                .withUrl("https://secure.snd.payu.com/pl/standard/user/oauth/authorize")
                .withRequestType("POST")
                .withXFormHeader()
                .withPostData("grant_type=client_credentials&client_id=337138&client_secret=eb8c773362a69d1b1eeb4a6bb137aa86")
                .build();
        String response=executePostRequest(request,false);
        AuthTokenResponse authTokenResponse=new Gson().fromJson(response,AuthTokenResponse.class);
        return authTokenResponse.getAccessToken();
    }

    private HttpURLConnection setBasicHeaders(HttpURLConnection con, String token) {
        con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        return con;
    }

    private HttpURLConnection setBasicHeadersWithToken(HttpURLConnection con, String token) {

        con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        con.setRequestProperty("Authorization", "Bearer "+token);
        return con;
    }

    private HttpURLConnection setXFormHeadersWithToken(HttpURLConnection con, String token) {

        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Authorization", "Bearer "+token);
        return con;
    }
    private HttpURLConnection setHeaders(HttpURLConnection con, Map<String, String> header) {
        for (Map.Entry<String, String> entry : header.entrySet()) {
            con.setRequestProperty(entry.getKey(),entry.getValue());
        }
        return con;
    }
  /*  public List<WSPaymentGatewayMaster> getAllPaymentGatewayMasterByAcademyLocationIds() throws IOException { //TODOrename it
        // HTTP Post request
        // String url = "http://localhost:8080/serosoft-academia-web"+"/rest/paymentGatewayMaster/findAllByAcademyLocationIds";
        String url = environment.getRequiredProperty("erpUrl")+"/rest/paymentGatewayMaster/findAllByAcademyLocationIds";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // Setting basic get request
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", "Bearer "+getAuthToken());

        // Send post request
        con.setInstanceFollowRedirects(false);
        con.setDoOutput(true);

        LOGGER.debug("nSending 'GET' request to URL : " + url);
        // LOGGER.debug("Post Data : " + postJsonData);
        LOGGER.debug("Response Data : " + con.getResponseMessage());
        LOGGER.debug("Response Code : " + con.getResponseCode());
        StringBuilder sb = new StringBuilder();
        switch (con.getResponseCode()) {
            case 200:
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                break;
            default :
                throw new RuntimeException("Unable to fetch payment gateway data");
        }

        WSPaymentGatewayMaster[] wsPaymentGatewayMasterArray = new Gson().fromJson(sb.toString(),WSPaymentGatewayMaster[].class);
        List<WSPaymentGatewayMaster> wsPaymentGatewayMasterList = Arrays.asList(wsPaymentGatewayMasterArray);
        return wsPaymentGatewayMasterList;
    }
    }*/



}
