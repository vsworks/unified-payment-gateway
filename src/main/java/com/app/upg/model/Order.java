package com.app.upg.model;

import com.app.upg.services.Gateway;

/**
 * Created by Hi on 09-08-2018.
 */
public class Order {

    Gateway gateway;
    int amount;
    String emailId;
    String firstName;
    String lastName;
    String notifyUrl;
    String description;
    String currency;
    String consumerUniqueKey;
    String customF1;

    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString()
    {
        return  gateway+ " : "+
                 " : "+amount +
        " : "+ emailId+
        " : "+  firstName+
        " : "+  lastName+
        " : "+  notifyUrl+
        " : "+  description+
        " : "+  currency+
        " : "+  consumerUniqueKey+
        " : "+  customF1;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getConsumerUniqueKey() {
        return consumerUniqueKey;
    }

    public void setConsumerUniqueKey(String consumerUniqueKey) {
        this.consumerUniqueKey = consumerUniqueKey;
    }

    public String getCustomF1() {
        return customF1;
    }

    public void setCustomF1(String customF1) {
        this.customF1 = customF1;
    }
}
