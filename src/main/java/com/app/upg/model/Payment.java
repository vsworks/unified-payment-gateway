package com.app.upg.model;

/**
 * Created by Hi on 09-08-2018.
 */
public class Payment {

    String status;
    String transcationId;
    double amount;
    String gateway;

    public Payment(String status, String transcationId, double amount, String gateway) {

        this.status = status;
        this.transcationId = transcationId;
        this.amount = amount;
        this.gateway = gateway;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTranscationId() {
        return transcationId;
    }

    public void setTranscationId(String transcationId) {
        this.transcationId = transcationId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }
}
